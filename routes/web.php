<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', [
  'uses' => 'PostController@getBlogIndex',
  'as' => 'blog.index'
]);

Route::get('/blog', [
  'uses' => 'PostController@getBlogIndex',
  'as' => 'blog.index'
]);

Route::get('/blog/{post_id}&{end}', [
  'uses' => 'PostController@getSinglePost',
  'as' => 'blog.single'
]);

Route::get('/about', function() {
  return view('frontend.other.about');
})->name('about');

Route::get('/contact', [
  'uses' => 'ContactMessageController@getContactIndex',
  'as' => 'contact'
]);

/*
 * Group of URL that will be under the prefix ADMIN
 */
Route::group(['prefix' => '/admin'], function() {
  Route::get('/', [
    'uses' => 'AdminController@getIndex',
    'as' => 'admin.index'
  ]);
  Route::get('/blog/posts', [
    'uses' => 'PostController@getPostIndex',
    'as' => 'admin.blog.index'
  ]);
  Route::get('blog/post/{post_id}&{end}', [
    'uses' => 'PostController@getSinglePost',
    'as' => 'admin.blog.post'
  ]);
  Route::get('/blog/posts/create', [
    'uses' => 'PostController@getCreatePost',
    'as' => 'admin.blog.create_posts'
  ]);
  Route::post('/blog/posts/create', [
    'uses' => 'PostController@postCreatePost',
    'as' => 'admin.blog.posts_create'
  ]);
  Route::get('/blog/posts/{post_id}/edit', [
    'uses' => 'PostController@getUpdatePost',
    'as' => 'admin.blog.post.edit'
  ]);
  Route::get('/blog/posts/update', [
    'uses' => 'PostController@postUpdatePost',
    'as' => 'admin.blog.post.update'
  ]);
  Route::get('blog/post/{post_id}/delete', [
    'uses' => 'PostController@getDeletePost',
    'as' => 'admin.blog.post.delete'
  ]);
  Route::get('blog/categories', [
    'uses' => 'CategoryController@getCategoryIndex',
    'as' => 'admin.blog.categories'
  ]);
});

Route::resource("tweets","TweetController");
