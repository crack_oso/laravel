<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;

class PostController extends Controller {

    /*
     * @Description Makes the paginator to display the posts for frontend
     * @Return      Array with posts (as a paginator)
     */
      public function getBlogIndex() {
        $posts = Post::paginate('5');
        foreach ($posts as $post) {
          $post->body = $this->shortenText($post->body, 20);
        }
        return view('frontend.blog.index', [ 'posts' => $posts ]);
      }

    /*
     * @Description Makes the paginator to display the posts for Admin
     * @Return      Array with posts (as a paginator)
     */
      public function getPostIndex() {
        $posts = Post::paginate();
        return view('admin.blog.index', [ 'posts' => $posts ]);
      }

    /*
     * @Param       Int Post ID
     * @Description Bring a post from ID
     * @Return      Array with post id
     */
      public function getSinglePost($post_id, $end = 'frontend') {
        $post = Post::find($post_id);
        if (!$post) {
          return redirect()->route('blog.index')->with(['fail' => 'Post not found']);
        }
        return view( $end . '.blog.single', ['post' => $post ] );
      }

    /*
     * @Description Display the form to create a new post
     * @Return      View
     */
      public function getCreatePost() {
        return view('admin.blog.create_posts');
      }

    /*
     * @Param       POST Request
     * @Description Receives the POST request if is valid will be saved in the database
     * @Return      Success if is created
     */
      public function postCreatePost(Request $request) {
        $this->validate($request, [
          'title' => 'required|max:120|unique:posts',
          'author' => 'required|max:80',
          'body' => 'required'
        ]);

        $post = new Post();
        $post->title = $request['title'];
        $post->author = $request['author'];
        $post->body = $request['body'];
        $post->save();

        return redirect()->route('admin.index')->with(['success' => 'Post created!']);
      }

    /*
     * @Param       Int Post ID
     * @Description Receives the post_id via GET to update the post
     * @Return      Array post
     */
      public function getUpdatePost($post_id) {
        $post = Post::find($post_id);
        if(!$post) {
          return redirect()->route('blog.index')->with(['fail' => 'Page not found :(']);
        }
        return view($end . 'blog.single' . ['post' => $post]);
      }

    /*
     * @Param       POST Request
     * @Description Receives the POST request if is valid will be updated in the database
     * @Return      Success if is created
     */
      public function postUpdatePost(Request $request) {
        $this->validate($request, [
          'title' => 'required|max:120',
          'author' => 'required|max:80',
          'body' => 'required'
        ]);
        $post = Post::find($request['post_id']);
        $post->title = $request['title'];
        $post->author = $request['author'];
        $post->body = $request['body'];
        $post->update();
        //Categories ...
        return redirect()->route('admin.index')->with(['success' => 'Post updated']);
      }

    /*
     * @Param       GET Request
     * @Description Receives the GET request if is found will be deleted from the database
     * @Return      Success if is deleted
     */
      public function getDeletePost($post_id) {
        $post = Post::find($post_id);
        if(!$post) {
          return redirect()->route('admin.index')->with(['fail' => 'Post not found :(']);
        }
        $post->delete();
        return redirect()->route('admin.index')->with(['success' => 'Post deleted']);
      }

    /*
     * @Param       String
     * @Description Cut a string and puts '...' if is too long
     * @Return      String cutted
     */
      private function shortenText($text, $words_count) {
        if (str_word_count($text, 0) > $words_count) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          $text = substr($text, 0, $pos[$words_count]) . '...';
        }
        return $text;
      }

}
