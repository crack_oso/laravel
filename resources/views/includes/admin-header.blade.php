<header class="top-nav">
  <nav>
    <ul>
      <li><i class="fa fa-tachometer"></i><a href="{{ route('admin.index') }}">Dashboard</a></li>
      <li><i class="fa fa-sticky-note"></i><a href="{{ route('admin.blog.index') }}">Posts</a></li>
      <li><i class="fa fa-folder-open"></i><a href="{{ route('admin.blog.categories') }}">Categories</a></li>
      <li><i class="fa fa-comment"></i><a href="#">Contact Messages</a></li>
      <li><i class="fa fa-lock"></i><a href="#">Log Out</a></li>
    </ul>
  </nav>
</header>
