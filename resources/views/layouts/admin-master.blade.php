<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Administration</title>
  @yield('styles')
  <link rel="stylesheet" href="{{ URL::to('css/main.css') }}">
  <link rel="stylesheet" href="{{ URL::to('css/font-awesome.min.css') }}">
</head>
<body>
  @include('includes.admin-header')
  @yield('content')

  <script type="text/javascript">
    var baseUrl = "{{ URL::to('/') }}";

  </script>
  @yield('scripts')
</body>
</html>
