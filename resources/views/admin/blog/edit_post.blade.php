@extends('layouts.admin-master')

@section('styles')
  <link rel="stylesheet" href="{{ URL::to('/css/form.css') }}">
@endsection

@section('content')
  <div class="container">
    @include('includes.info-box')
    <form action="{{ route('admin.blog.post_update') }}" method="post">
      <div class="input-group">
        <label for="title">Title</label>
        <input type="text" name="title" id="title" {{ $errors->has('title') ? 'class="has-error"' : '' }} value="{{ Request::old('title') ? Request::old('title') : isset($post) ? $post->title : '' }}">
      </div>
      <div class="input-group">
        <label for="author">Author</label>
        <input type="text" name="author" id="author" {{ $errors->has('title') ? 'class="has-error"' : '' }} value="{{ Request::old('author') ? Request::old('author') : isset($post) ? $post->author : '' }}">
      </div>
      <div class="input-group">
        <label for="category_select">Author</label>
        <select name="category_select" id="category_select" value="{{  Request::old('category_select') ? Request::old('category_select') : isset($post) ? $post->category_select : '' }}">
          <option value="Dummy Category">Dummy category</option>
        </select>
        <button type="button" class="btn">Add Category</button>
        <div class="added-categories">
          <ul>

          </ul>
        </div>
        <input type="hidden" name="categories" id="categories">
      </div>
      <div class="input-group">
        <label for="body">Body</label>
        <textarea name="body" id="body" rows="12">{{  Request::old('body') ? Request::old('body') : isset($post) ? $post->body : ''  }}</textarea>
      </div>
      <button type="submit" class="btn">Save Post</button>
      <input type="hidden" name="_token" value="{{ Session::token() }}">
      <input type="hidden" name="post_id" value="{{ $post->id }}">
    </form>
  </div>
@endsection
