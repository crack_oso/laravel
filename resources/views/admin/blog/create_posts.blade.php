@extends('layouts.admin-master')

@section('styles')
  <link rel="stylesheet" href="{{ URL::to('/css/form.css') }}">
@endsection

@section('content')
  <div class="container">
    @include('includes.info-box')
    <form action="{{ route('admin.blog.posts_create') }}" method="post" class="create-post">
      <h2>Create a new post</h2>
      <div class="input-group">
        <label for="title">Title</label>
        <input type="text" name="title" id="title" {{ $errors->has('title') ? 'class="has-error"' : '' }} value="{{ Request::old('title') }}">
      </div>
      <div class="input-group">
        <label for="author">Author</label>
        <input type="text" name="author" id="author" {{ $errors->has('title') ? 'class="has-error"' : '' }} value="{{ Request::old('author') }}">
      </div>
      <div class="input-group">
        <label for="category_select">Category</label>
        <select name="category_select" id="category_select" value="{{ Request::old('category_select') }}">
          <option value="Dummy Category">Dummy category</option>
        </select>
        <button type="button" class="btn">Add Category</button>
        <div class="added-categories">
          <ul>

          </ul>
        </div>
        <input type="hidden" name="categories" id="categories">
      </div>
      <div class="input-group">
        <label for="body">Body</label>
        <textarea name="body" id="body" rows="12">{{ Request::old('body') }}</textarea>
      </div>
      <button type="submit" class="btn submit">Create</button>
      <input type="hidden" name="_token" value="{{ Session::token() }}">
    </form>
  </div>
@endsection

@section('scripts')
  <script src="text/javascript" src="{{ URL::secure('/js/posts.js') }}"></script>
@endsection
