@extends('layouts.admin-master')

@section('styles')
  <link rel="stylesheet" href="/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{ URL::to('/css/categories.css') }}">
@endsection
@section('content')
  <div class="container">
    <section id="category-admin">
      <form method="post">
        <div class="input-group">
          <label for="name">Category name</label>
          <input type="text" id="name" name="name">
          <button type="submit" class="btn">Create category</button>
        </div>
      </form>
    </section>
    <section class="list">
      @foreach ($categories as $category)
        <article>
          <div class="category-info" data-id="{{ $category->id }}">
            <h3>{{ $category->name }}</h3>
          </div>
          <nav>
            <ul>
              <li><input type="text" class="category-edit" /></li>
              <li><a href="#">Edit</a></li>
              <li><a href="#" class="danger">Delete</a></li>
            </ul>
          </nav>
        </article>
      @endforeach
    </section>
    @if ($categories->lastpage() > 1 )
      <section class="pagination">
        @if ($categories->currentPage() !== 1)
          <a href="{{ $categories->previousPageUrl() }}"><i class="fa fa-caret-left"></i></a>
        @endif
        @if ($categories->currentPage() !== $categories->lastPage())
          <a href="{{ $categories->nextPageUrl() }}"><i class="fa fa-caret-right"></i></a>
        @endif
      </section>
    @endif
  </div>
@endsection

@section('scripts')
  <script type="text/javascript">
    var token = "{{ Session::token() }}";
  </script>
  <script src="{{ URL::to('js/categories.js') }}"></script>
@endsection
