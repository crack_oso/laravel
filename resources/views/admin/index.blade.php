@extends('layouts.admin-master')

@section('styles')
  <link rel="stylesheet" href="{{ URL::to('/css/modal.css') }}">
@endsection

@section('scripts')
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <script src="{{ URL::to('/js/app.js')}}"></script>
@endsection

@section('content')
  <div class="container">
    @include('includes.info-box')
    <div class="card">
      <div id='app'>@{{ message }}</div>
      <header>
        <nav>
          <ul>
            <li><a href="{{ route('admin.blog.create_posts') }}" class="btn">New post</a></li>
            <li><a href="{{ route('admin.blog.index') }}" class="btn">Show all posts</a></li>
          </ul>
        </nav>
      </header>
      <section>
        @if(count($posts) == 0)
          <h2>No posts</h2>
                @else
        @foreach ($posts as $post)
          <article>
            <div class="post-info">
              <h3>{{ $post->title }}</h3>
              <span class="info">{{ $post->author }} | {{ $post->created_at }}</span>
            </div>
            <div class="edit">
              <nav>
                <ul>
                  <li><a href="{{ route('admin.blog.post', ['post_id' => $post->id, 'end' => 'admin']) }}">View Post</a></li>
                  <li><a href="{{ route('admin.blog.post.edit', [ 'post_id' => $post->id ]) }}">Edit</a></li>
                  <li><a href="{{ route('admin.blog.post.delete', [ 'post_id' => $post->id ]) }}" class="danger">Delete</a></li>
                </ul>
              </nav>
            </div>
          </article>
        @endforeach
        @endif
      </section>
    </div>
    <div class="card">
      <header>
        <nav>
          <ul>
            <li><a href="" class="btn">Show all messages</a></li>
          </ul>
        </nav>
      </header>
      <section>
        <!-- If no posts -->
        <h2>No messages</h2>
        <!-- If posts -->
        <article data-message="Body" data-id="ID">
          <div class="message-info">
            <h3>Message subject</h3>
            <span class="info">Sender | Date</span>
          </div>
          <div class="edit">
            <nav>
              <ul>
                <li><a href="">View Message</a></li>
                <li><a href="" class="danger">Delete</a></li>
              </ul>
            </nav>
          </div>
        </article>
      </section>
    </div>
  </div>
  <div class="modal" id="contact-message-info">
    <button class="btn" id="modal-close">Close</button>
  </div>
@endsection

@section('scripts')
  <script>
    var token = "{{ Session::token() }}";
  </script>
  <script src="{{ URL::secure('src/js/modal.js') }}"></script>
  <script src="{{ URL::secure('src/js/contact_messages.js') }}"></script>
@endsection
